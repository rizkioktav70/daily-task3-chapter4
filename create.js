const fs = require("fs")
const createHuman = function (human) {
    fs.writeFileSync('./human.json', JSON.stringify(human))
    return human;
}

const rizki = createHuman(
    {
        name: 'Rizki Oktavianus',
        age: 21,
        address: 'Your heart',
    })

module.exports = createHuman
